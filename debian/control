Source: truffle
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper-compat (= 13), default-jdk, maven-repo-helper, javahelper
Standards-Version: 4.5.1
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/truffle.git
Vcs-Browser: https://salsa.debian.org/java-team/truffle
Homepage: http://openjdk.java.net/projects/graal

Package: libtruffle-java
Architecture: all
Depends: ${misc:Depends}
Suggests: libtruffle-java-doc
Description: multi-language framework for executing dynamic languages
 Truffle is a language abstract syntax tree interpreter which
 allow it to implement languages on top of the Graal framework.
 .
 To implement a language using Truffle you write an AST for your
 language and add methods to interpret (perform the action of) each
 node.
 .
 Graal is an Oracle project aiming to implement a high performance
 Java dynamic compiler and interpreter.

Package: libtruffle-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Suggests: libtruffle-java
Description: Documentation for truffle
 Truffle is a language abstract syntax tree interpreter which
 allow it to implement languages on top of the Graal framework.
 .
 To implement a language using Truffle you write an AST for your
 language and add methods to interpret (perform the action of) each
 node.
 .
 Graal is an Oracle project aiming to implement a high performance
 Java dynamic compiler and interpreter.
 .
 This package contains the API documentation of libtruffle-java.
